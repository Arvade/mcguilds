package org.bitbucket.arvade.mcguilds;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.mcregistrator.CommandRegistrar;
import org.bukkit.plugin.java.JavaPlugin;

public class McguildPlugin extends JavaPlugin {

    private Injector injector;

    @Override
    public void onEnable() {
        McguildModule module = new McguildModule(this);
        this.injector = Guice.createInjector(module);
        this.saveDefaultConfig();
        registerCommands();
    }

    private void registerCommands() {
        CommandRegistrar commandRegistrar = this.injector.getInstance(CommandRegistrar.class);
        commandRegistrar.registerCommands("org.bitbucket.arvade.mcguilds.command");
    }
}
