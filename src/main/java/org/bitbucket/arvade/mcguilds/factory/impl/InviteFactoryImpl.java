package org.bitbucket.arvade.mcguilds.factory.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.mcguilds.factory.InviteFactory;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.model.Invite;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcguilds.service.InviteService;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.Optional;

public class InviteFactoryImpl implements InviteFactory {

    private InviteService inviteService;
    private GuildService guildService;
    private GuildMemberService guildMemberService;

    @Inject
    public InviteFactoryImpl(InviteService inviteService, GuildService guildService, GuildMemberService guildMemberService) {
        this.inviteService = inviteService;
        this.guildService = guildService;
        this.guildMemberService = guildMemberService;
    }

    @Override
    public boolean createInvite(Player invitator, Player target, Guild guild) {
        Optional<Guild> targetsGuild = guildService.findByUUID(target.getUniqueId());

        boolean validationResult = true;

        if (targetsGuild.isPresent()) {
            invitator.sendMessage(ChatColor.RED + "Player " + ChatColor.BLUE + target.getDisplayName() + ChatColor.RED + " has already guild!");
            validationResult = false;
        }

        Optional<GuildMember> targetGuildMember = guildMemberService.findByUUID(target.getUniqueId());

        if (targetGuildMember.isPresent()) {
            GuildMember guildMember = targetGuildMember.get();
            if (guildMember.getGuild() != null) {
                invitator.sendMessage(ChatColor.RED + "Player " + ChatColor.BLUE + target.getDisplayName() + ChatColor.RED + " is already a member of some guild");
                validationResult = false;
            }
        }

        Optional<Invite> latestInviteOptional = inviteService.findLatestInviteByBothUUIDs(invitator.getUniqueId(), target.getUniqueId());

        if (validationResult) {
            if (latestInviteOptional.isPresent()) {
                Invite invite = latestInviteOptional.get();

                if (invite.isAccepted()) {
                    sendInvite(invitator, target, guild);
                } else {
                    Date requestDate = invite.getRequestDate();
                    Date currentDate = new Date();

                    long diff = currentDate.getTime() - requestDate.getTime();
                    long diffInMinutes = (diff / 60000);

                    if (diffInMinutes > 5) {
                        sendInvite(invitator, target, guild);
                    } else {
                        validationResult = false;
                    }
                }
            } else {
                sendInvite(invitator, target, guild);
            }
        }


        return validationResult;
    }

    private void sendInvite(Player invitator, Player target, Guild guild) {
        Invite invite = new Invite();
        invite.setInvitator(invitator.getUniqueId());
        invite.setTarget(target.getUniqueId());
        invite.setGuild(guild);
        inviteService.persist(invite);
    }
}
