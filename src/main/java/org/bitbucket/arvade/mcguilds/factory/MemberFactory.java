package org.bitbucket.arvade.mcguilds.factory;

import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bukkit.entity.Player;

public interface MemberFactory {
    
    GuildMember createGuildMember(Player player, Guild guild);
}
