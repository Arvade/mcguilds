package org.bitbucket.arvade.mcguilds.factory.impl;

import org.bitbucket.arvade.mcguilds.factory.MemberFactory;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bukkit.entity.Player;

import javax.inject.Inject;

public class MemberFactoryImpl implements MemberFactory {

    private GuildMemberService guildMemberService;

    @Inject
    public MemberFactoryImpl(GuildMemberService guildMemberService) {
        this.guildMemberService = guildMemberService;
    }

    @Override
    public GuildMember createGuildMember(Player player, Guild guild) {
        GuildMember guildMember = new GuildMember();
        guildMember.setGuild(guild);
        guildMember.setNickname(player.getDisplayName());
        guildMember.setPlayerUUID(player.getUniqueId());

        guildMemberService.persist(guildMember);

        return guildMember;
    }
}
