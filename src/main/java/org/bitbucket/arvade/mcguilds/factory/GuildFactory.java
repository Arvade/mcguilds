package org.bitbucket.arvade.mcguilds.factory;

import org.bukkit.entity.Player;

public interface GuildFactory {

    boolean createGuild(Player owner, String guildName);
}
