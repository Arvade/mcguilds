package org.bitbucket.arvade.mcguilds.factory.impl;

import org.bitbucket.arvade.mcguilds.factory.GuildFactory;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

public class GuildFactoryImpl implements GuildFactory {

    private GuildService guildService;
    private GuildMemberService guildMemberService;

    @Inject
    public GuildFactoryImpl(GuildService guildService, GuildMemberService guildMemberService) {
        this.guildService = guildService;
        this.guildMemberService = guildMemberService;
    }

    @Override
    public boolean createGuild(Player owner, String guildName) {
        Optional<Guild> hasGuildAlready = guildService.findByUUID(owner.getUniqueId());
        Optional<GuildMember> guildMemberOptional = guildMemberService.findByUUID(owner.getUniqueId());
        Optional<Guild> isGuildWithGivenNameExists = guildService.findByName(guildName);

        boolean validationResult = true;

        if (hasGuildAlready.isPresent()) {
            Guild guild = hasGuildAlready.get();
            owner.sendMessage(ChatColor.RED + "You're already owner of " + ChatColor.BLUE + guild.getGuildName() + ChatColor.RED + " guild!");
            validationResult = false;
        }

        if (guildMemberOptional.isPresent()) {
            GuildMember guildMember = guildMemberOptional.get();
            Guild guild = guildMember.getGuild();
            if (guild != null) {
                owner.sendMessage(ChatColor.RED + "You're already member of " + ChatColor.BLUE + guildMember.getGuild().getGuildName() + ChatColor.RED + " guild!");
                validationResult = false;
            }
        }

        if (isGuildWithGivenNameExists.isPresent()) {
            Guild guild = isGuildWithGivenNameExists.get();
            if (!guild.isClosed()) {
                owner.sendMessage(ChatColor.RED + "Guild with given name already exists! (" + ChatColor.BLUE + guild.getGuildName() + ChatColor.RED + ")");
                validationResult = false;
            }
        }

        if (validationResult) {
            Guild guild = new Guild();
            guild.setOwnerUUID(owner.getUniqueId());
            guild.setGuildName(guildName);
            guild.setOwnerNickname(owner.getDisplayName());
            guild.setClosed(false);
            guildService.persist(guild);
        }

        return validationResult;
    }
}
