package org.bitbucket.arvade.mcguilds.factory;

import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bukkit.entity.Player;

public interface InviteFactory {
    boolean createInvite(Player invitator, Player target, Guild guild);
}
