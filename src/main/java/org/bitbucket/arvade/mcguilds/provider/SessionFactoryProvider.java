package org.bitbucket.arvade.mcguilds.provider;

import lombok.Setter;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.Collection;

public class SessionFactoryProvider implements Provider<SessionFactory> {

    private static final String DEFAULT_ENTITY_PACKAGE = "org.bitbucket.arvade.mcguilds.model";

    @Setter
    private String entityPackage = DEFAULT_ENTITY_PACKAGE;

    private Configuration configuration = new Configuration();
    private PackageScanner packageScanner;

    @Inject
    public SessionFactoryProvider(PackageScanner packageScanner) {
        this.packageScanner = packageScanner;
    }

    @Override
    public SessionFactory get() {
        Collection<Class<?>> classes = packageScanner.findClasses(entityPackage);

        classes.forEach(aClass -> configuration.addAnnotatedClass(aClass));

        configuration.configure();
        ServiceRegistry sr = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(sr);
    }
}
