package org.bitbucket.arvade.mcguilds.service.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.Invite;
import org.bitbucket.arvade.mcguilds.repository.InviteRepository;
import org.bitbucket.arvade.mcguilds.service.InviteService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class InviteServiceImpl implements InviteService {

    private InviteRepository inviteRepository;

    @Inject
    public InviteServiceImpl(InviteRepository inviteRepository) {
        this.inviteRepository = inviteRepository;
    }

    @Override
    public void persist(Invite invite) {
        inviteRepository.persist(invite);
    }

    @Override
    public void delete(Invite invite) {
        inviteRepository.delete(invite);
    }

    @Override
    public Optional<Invite> findById(Integer id) {
        return inviteRepository.findById(id);
    }

    @Override
    public List<Invite> findAll() {
        return inviteRepository.findAll();
    }

    @Override
    public Optional<Invite> findByInvitatorUUID(UUID uuid) {
        return inviteRepository.findByInvitatorUUID(uuid);
    }

    @Override
    public Optional<Invite> findByTargetUUID(UUID uuid) {
        return inviteRepository.findByTargetUUID(uuid);
    }

    @Override
    public Optional<Invite> findLatestInviteByBothUUIDs(UUID invitator, UUID target) {
        return inviteRepository.findLatestInviteByBothUUIDs(invitator, target);
    }

    @Override
    public Optional<Invite> findLatestInvite(UUID target, Guild guild) {
        return inviteRepository.findLatestInvite(target, guild);
    }
}
