package org.bitbucket.arvade.mcguilds.service.impl;

import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.repository.GuildMemberRepository;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GuildMemberServiceImpl implements GuildMemberService {

    private GuildMemberRepository guildMemberRepository;

    @Inject
    public GuildMemberServiceImpl(GuildMemberRepository guildMemberRepository) {
        this.guildMemberRepository = guildMemberRepository;
    }

    @Override
    public void persist(GuildMember guildMember) {
        guildMemberRepository.persist(guildMember);
    }

    @Override
    public void delete(GuildMember guildMember) {
        guildMemberRepository.delete(guildMember);
    }

    @Override
    public Optional<GuildMember> findById(Integer id) {
        return guildMemberRepository.findById(id);
    }

    @Override
    public List<GuildMember> findAll() {
        return guildMemberRepository.findAll();
    }

    @Override
    public Optional<GuildMember> findByName(String name) {
        return guildMemberRepository.findByName(name);
    }

    @Override
    public Optional<GuildMember> findByUUID(UUID uuid) {
        return guildMemberRepository.findByUUID(uuid);
    }

    @Override
    public void exitGuild(GuildMember guildMember) {
        GuildMember member = new GuildMember();
        member.setPlayerUUID(guildMember.getPlayerUUID());
        member.setNickname(guildMember.getNickname());
        member.setJoinDate(new Date());
        member.setGuild(null);
        member.setMeritPoints(0);
        this.guildMemberRepository.persist(member);
    }

}
