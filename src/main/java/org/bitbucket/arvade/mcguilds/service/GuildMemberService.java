package org.bitbucket.arvade.mcguilds.service;

import org.bitbucket.arvade.mcguilds.model.GuildMember;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GuildMemberService {
    void persist(GuildMember guildMember);

    void delete(GuildMember guildMember);

    Optional<GuildMember> findById(Integer id);

    List<GuildMember> findAll();

    Optional<GuildMember> findByName(String name);

    Optional<GuildMember> findByUUID(UUID uuid);

    void exitGuild(GuildMember guildMember);
}
