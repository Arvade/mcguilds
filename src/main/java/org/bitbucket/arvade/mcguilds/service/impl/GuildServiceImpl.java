package org.bitbucket.arvade.mcguilds.service.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.repository.GuildRepository;
import org.bitbucket.arvade.mcguilds.service.GuildService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GuildServiceImpl implements GuildService {

    private GuildRepository guildRepository;

    @Inject
    public GuildServiceImpl(GuildRepository guildRepository) {
        this.guildRepository = guildRepository;
    }

    @Override
    public void persist(Guild guild) {
        guildRepository.persist(guild);
    }

    @Override
    public void delete(Guild guild) {
        guild.setClosed(true);
        guildRepository.persist(guild);
    }

    @Override
    public Optional<Guild> findById(Long id) {
        return guildRepository.findById(id);
    }

    @Override
    public List<Guild> findAll() {
        return guildRepository.findAll();
    }

    @Override
    public Optional<Guild> findByName(String name) {
        return guildRepository.findByName(name);
    }

    @Override
    public Optional<Guild> findByUUID(UUID ownerUUID) {
        return guildRepository.findByUUID(ownerUUID);
    }
}
