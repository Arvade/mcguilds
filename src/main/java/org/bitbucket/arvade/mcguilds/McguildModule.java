package org.bitbucket.arvade.mcguilds;

import com.google.inject.AbstractModule;
import org.bitbucket.arvade.mcguilds.factory.GuildFactory;
import org.bitbucket.arvade.mcguilds.factory.InviteFactory;
import org.bitbucket.arvade.mcguilds.factory.MemberFactory;
import org.bitbucket.arvade.mcguilds.factory.impl.GuildFactoryImpl;
import org.bitbucket.arvade.mcguilds.factory.impl.InviteFactoryImpl;
import org.bitbucket.arvade.mcguilds.factory.impl.MemberFactoryImpl;
import org.bitbucket.arvade.mcguilds.provider.SessionFactoryProvider;
import org.bitbucket.arvade.mcguilds.repository.GuildMemberRepository;
import org.bitbucket.arvade.mcguilds.repository.GuildRepository;
import org.bitbucket.arvade.mcguilds.repository.InviteRepository;
import org.bitbucket.arvade.mcguilds.repository.impl.GuildMemberRepositoryImpl;
import org.bitbucket.arvade.mcguilds.repository.impl.GuildRepositoryImpl;
import org.bitbucket.arvade.mcguilds.repository.impl.InviteRepositoryImpl;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcguilds.service.InviteService;
import org.bitbucket.arvade.mcguilds.service.impl.GuildMemberServiceImpl;
import org.bitbucket.arvade.mcguilds.service.impl.GuildServiceImpl;
import org.bitbucket.arvade.mcguilds.service.impl.InviteServiceImpl;
import org.bitbucket.arvade.mcregistrator.CommandRegistrar;
import org.bitbucket.arvade.mcregistrator.EventRegistrar;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.bitbucket.arvade.mcregistrator.impl.CommandRegistrarImpl;
import org.bitbucket.arvade.mcregistrator.impl.EventRegistrarImpl;
import org.bitbucket.arvade.mcregistrator.impl.PackageScannerImpl;
import org.bukkit.plugin.java.JavaPlugin;
import org.hibernate.SessionFactory;

public class McguildModule extends AbstractModule {

    private JavaPlugin plugin;

    public McguildModule(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    protected void configure() {
        bind(JavaPlugin.class).toInstance(plugin);
        bind(PackageScanner.class).to(PackageScannerImpl.class);
        bind(SessionFactory.class).toProvider(SessionFactoryProvider.class);

        bind(CommandRegistrar.class).to(CommandRegistrarImpl.class);
        bind(EventRegistrar.class).to(EventRegistrarImpl.class);
        bind(GuildFactory.class).to(GuildFactoryImpl.class);
        bind(MemberFactory.class).to(MemberFactoryImpl.class);
        bind(GuildMemberService.class).to(GuildMemberServiceImpl.class);
        bind(GuildService.class).to(GuildServiceImpl.class);
        bind(GuildMemberRepository.class).to(GuildMemberRepositoryImpl.class);
        bind(GuildRepository.class).to(GuildRepositoryImpl.class);
        bind(InviteRepository.class).to(InviteRepositoryImpl.class);
        bind(InviteService.class).to(InviteServiceImpl.class);
        bind(InviteFactory.class).to(InviteFactoryImpl.class);
    }
}
