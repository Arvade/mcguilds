package org.bitbucket.arvade.mcguilds.repository.impl;

import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.repository.GuildMemberRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GuildMemberRepositoryImpl implements GuildMemberRepository {

    private SessionFactory sessionFactory;

    @Inject
    public GuildMemberRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void persist(GuildMember guild) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(guild);
        transaction.commit();
    }

    @Override
    public void delete(GuildMember guild) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.delete(guild);
        transaction.commit();
    }

    @Override
    public Optional<GuildMember> findById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        GuildMember guild = null;

        try {
            guild = session.createQuery("from GuildMember where id = :id", GuildMember.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.WARNING, "No guildMember found for id " + id);
        }

        transaction.commit();
        return Optional.ofNullable(guild);
    }

    @Override
    public List<GuildMember> findAll() {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        List<GuildMember> guildList = session.createQuery("from GuildMember", GuildMember.class)
                .getResultList();

        transaction.commit();
        return guildList;
    }

    @Override
    public Optional<GuildMember> findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        GuildMember guildMember = null;
        try {
            guildMember = session
                    .createQuery("from GuildMember where nickname = :nickname", GuildMember.class)
                    .setParameter("nickname", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No guild found for player name " + name);
        }
        transaction.commit();

        return Optional.ofNullable(guildMember);
    }

    @Override
    public Optional<GuildMember> findByUUID(UUID uuid) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        GuildMember guildMember = null;
        try {
            guildMember = session
                    .createQuery("FROM GuildMember WHERE playerUUID = :playerUUID ORDER BY joinDate desc", GuildMember.class)
                    .setMaxResults(1)
                    .setParameter("playerUUID", uuid)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No guild found for player uuid " + uuid);
        }
        transaction.commit();
        return Optional.ofNullable(guildMember);
    }
}
