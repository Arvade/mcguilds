package org.bitbucket.arvade.mcguilds.repository.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.Invite;
import org.bitbucket.arvade.mcguilds.repository.InviteRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InviteRepositoryImpl implements InviteRepository {

    private SessionFactory sessionFactory;

    @Inject
    public InviteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void persist(Invite invite) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(invite);
        transaction.commit();
    }

    @Override
    public void delete(Invite invite) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.delete(invite);
        transaction.commit();
    }

    @Override
    public Optional<Invite> findById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Invite invite = null;
        try {
            invite = session.createQuery("from Invite where id = :id", Invite.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No invite found for id " + id, e);
        }

        transaction.commit();
        return Optional.ofNullable(invite);
    }

    @Override
    public List<Invite> findAll() {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        List<Invite> inviteList = new ArrayList<>();
        try {
            inviteList = session
                    .createQuery("from Invite", Invite.class)
                    .getResultList();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No invites found", e);
        }

        transaction.commit();
        return inviteList;
    }

    @Override
    public Optional<Invite> findByInvitatorUUID(UUID uuid) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Invite invite = null;
        try {
            invite = session
                    .createQuery("from Invite where invitator = :invitator", Invite.class)
                    .setParameter("invitator", uuid)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No invite found for invitator's uuid " + uuid);
        }

        transaction.commit();
        return Optional.ofNullable(invite);
    }

    @Override
    public Optional<Invite> findByTargetUUID(UUID uuid) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Invite invite = null;
        try {
            invite = session
                    .createQuery("from Invite where target = :target", Invite.class)
                    .setParameter("target", uuid)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No invite found for target uuid " + uuid);
        }

        transaction.commit();
        return Optional.ofNullable(invite);
    }

    @Override
    public Optional<Invite> findLatestInviteByBothUUIDs(UUID invitator, UUID target) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Invite invite = null;
        try {
            invite = session
                    .createQuery("from Invite where invitator = :invitator and target = :target order by requestDate desc", Invite.class)
                    .setMaxResults(1)
                    .setParameter("invitator", invitator)
                    .setParameter("target", target)
                    .getSingleResult();
        } catch (NoResultException e) {
            Logger.getAnonymousLogger().log(Level.INFO, "No lastest invite found for both invitator and target uuid " + invitator + " " + target, e);
        }

        transaction.commit();
        return Optional.ofNullable(invite);
    }

    @Override
    public Optional<Invite> findLatestInvite(UUID target, Guild guild) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Invite invite = null;
        try {
            invite = session
                    .createQuery("FROM Invite WHERE target = :target AND guild = :guild order by requestDate desc", Invite.class)
                    .setMaxResults(1)
                    .setParameter("target", target)
                    .setParameter("guild", guild)
                    .getSingleResult();
        } catch (NoResultException ignored) {
            Logger.getAnonymousLogger().log(Level.INFO, "No latest invite found for " + target + " from guild " + guild.getGuildName());
        }
        transaction.commit();
        return Optional.ofNullable(invite);
    }
}
