package org.bitbucket.arvade.mcguilds.repository.impl;

import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.repository.GuildRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class GuildRepositoryImpl implements GuildRepository {

    private SessionFactory sessionFactory;

    @Inject
    public GuildRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void persist(Guild guild) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(guild);
        transaction.commit();
    }

    @Override
    public void delete(Guild guild) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.delete(guild);
        transaction.commit();
    }

    @Override
    public Optional<Guild> findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Guild guild = null;

        try {
            guild = session.createQuery("from Guild where id = :id", Guild.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
        }

        transaction.commit();
        return Optional.ofNullable(guild);
    }

    @Override
    public List<Guild> findAll() {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        List<Guild> guildList = session.createQuery("from Guild", Guild.class)
                .getResultList();

        transaction.commit();
        return guildList;
    }

    @Override
    public Optional<Guild> findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Guild guild = null;
        try {
            guild = session.createQuery("from Guild where guildName = :guildName", Guild.class)
                    .setParameter("guildName", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
        }

        transaction.commit();
        return Optional.ofNullable(guild);
    }

    @Override
    public Optional<Guild> findByUUID(UUID ownerUUID) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Guild guild = null;
        try {
            guild = session
                    .createQuery("FROM Guild WHERE ownerUUID = :ownerUUID AND isClosed = false ORDER BY creationDate desc", Guild.class)
                    .setMaxResults(1)
                    .setParameter("ownerUUID", ownerUUID)
                    .getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
        }

        transaction.commit();
        return Optional.ofNullable(guild);
    }
}
