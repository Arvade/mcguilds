package org.bitbucket.arvade.mcguilds.repository;

import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.Invite;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface InviteRepository {

    void persist(Invite invite);

    void delete(Invite invite);

    Optional<Invite> findById(Integer id);

    List<Invite> findAll();

    Optional<Invite> findByInvitatorUUID(UUID uuid);

    Optional<Invite> findByTargetUUID(UUID uuid);

    Optional<Invite> findLatestInviteByBothUUIDs(UUID invitator, UUID target);

    Optional<Invite> findLatestInvite(UUID target, Guild guild);
}
