package org.bitbucket.arvade.mcguilds.repository;

import org.bitbucket.arvade.mcguilds.model.Guild;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GuildRepository {
    void persist(Guild guild);

    void delete(Guild guild);

    Optional<Guild> findById(Long id);

    List<Guild> findAll();

    Optional<Guild> findByName(String name);

    Optional<Guild> findByUUID(UUID uuid);
}
