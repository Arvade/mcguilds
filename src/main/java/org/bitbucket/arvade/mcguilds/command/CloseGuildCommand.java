package org.bitbucket.arvade.mcguilds.command;

import lombok.Setter;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.inject.Inject;
import java.util.Optional;

public class CloseGuildCommand implements NamedCommandExecutor {

    private final String COMMAND_NAME = "closeguild";

    @Setter
    private String commandName = COMMAND_NAME;

    private GuildService guildService;

    @Inject
    public CloseGuildCommand(GuildService guildService) {
        this.guildService = guildService;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command!");
            return true;
        }
        if (args.length != 0) {
            commandSender.sendMessage(ChatColor.RED + "Wrong amount of arguments!");
            return false;
        }

        Player player = (Player) commandSender;

        Optional<Guild> guildOptional = guildService.findByUUID(player.getUniqueId());
        if (!guildOptional.isPresent()) {
            player.sendMessage(ChatColor.RED + "You're not owner of any guild!");
            return true;
        }
        Guild guild = guildOptional.get();
        player.sendMessage(ChatColor.GREEN + "Guild " + ChatColor.BLUE + guild.getGuildName() + ChatColor.GREEN + " has been successfully closed!");
        guildService.delete(guild);


        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
