package org.bitbucket.arvade.mcguilds.command;

import com.google.inject.Inject;
import lombok.Setter;
import org.bitbucket.arvade.mcguilds.factory.InviteFactory;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

@SuppressWarnings("ConstantConditions")
public class InviteGuildCommand implements NamedCommandExecutor {

    private final String DEFAULT_NAME = "inviteguild";

    @Setter
    private String commandName = DEFAULT_NAME;

    private JavaPlugin plugin;
    private InviteFactory inviteFactory;
    private GuildService guildService;

    @Inject
    public InviteGuildCommand(JavaPlugin plugin, InviteFactory inviteFactory, GuildService guildService) {
        this.plugin = plugin;
        this.inviteFactory = inviteFactory;
        this.guildService = guildService;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command!");
            return true;
        }

        if (args.length != 1) {
            commandSender.sendMessage(ChatColor.RED + "Wrong amount of arguments!");
            return false;
        }

        Player invitator = (Player) commandSender;
        Player target = plugin.getServer().getPlayer(args[0]);

        if (target == null) {
            invitator.sendMessage(ChatColor.BLUE + args[0] + ChatColor.RED + " doesn't exists!");
            return true;
        }
        if (!target.isOnline()) {
            invitator.sendMessage(ChatColor.BLUE + args[0] + ChatColor.RED + " is not online!");
            return true;
        }
        Optional<Guild> invitatorGuildOptional = guildService.findByUUID(invitator.getUniqueId());

        if (!invitatorGuildOptional.isPresent()) {
            invitator.sendMessage(ChatColor.RED + "You need to be leader of guild!");
            return true;
        }
        Guild invitatorsGuild = invitatorGuildOptional.get();

        if (inviteFactory.createInvite(invitator, target, invitatorsGuild)) {
            invitator.sendMessage(ChatColor.GREEN + "You successfully invited " + ChatColor.BLUE + target.getDisplayName());
            target.sendMessage(ChatColor.BLUE + invitator.getDisplayName() + ChatColor.GREEN + " invited you to guild " + ChatColor.BLUE + invitatorsGuild.getGuildName());
            target.sendMessage(ChatColor.GREEN + "Type: " + ChatColor.GOLD + " /acceptinvite " + ChatColor.BLUE + invitatorsGuild.getGuildName() + ChatColor.GREEN + " to accept this invitation");
        }

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
