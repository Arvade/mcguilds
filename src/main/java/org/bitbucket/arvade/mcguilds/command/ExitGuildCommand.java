package org.bitbucket.arvade.mcguilds.command;

import lombok.Setter;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import javax.inject.Inject;
import java.util.Optional;

public class ExitGuildCommand implements NamedCommandExecutor {

    private static final String DEFAULT_NAME = "exitguild";

    @Setter
    private String commandName = DEFAULT_NAME;

    private GuildService guildService;
    private GuildMemberService guildMemberService;
    private JavaPlugin javaPlugin;

    @Inject
    public ExitGuildCommand(GuildService guildService, GuildMemberService guildMemberService, JavaPlugin javaPlugin) {
        this.guildService = guildService;
        this.guildMemberService = guildMemberService;
        this.javaPlugin = javaPlugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command!");
            return true;
        }
        if (args.length > 0) {
            commandSender.sendMessage(ChatColor.RED + "Wrong amount of arguments!");
            return false;
        }

        Player player = (Player) commandSender;

        Optional<Guild> isOwnerOfGuild = guildService.findByUUID(player.getUniqueId());
        if (isOwnerOfGuild.isPresent()) {
            player.sendMessage(ChatColor.RED + "You cannot exit guild if you're a leader!");
            return true;
        }

        Optional<GuildMember> guildMemberOptional = guildMemberService.findByUUID(player.getUniqueId());
        if (!guildMemberOptional.isPresent()) {
            player.sendMessage(ChatColor.RED + "You cannot exit guild if you don't have one!");
            return true;
        }

        GuildMember guildMember = guildMemberOptional.get();
        Guild guild = guildMember.getGuild();
        this.guildMemberService.exitGuild(guildMember);

        //TODO: Ustawienie nowego GuildMember i wrzucenie do bazy
        //TODO: !!!!!  Wyszukiwanie wszystkich GuildMembers dla Guild'a bedzie odbywac sie poprzez najnowsza date dla UUID !!!

        player.sendMessage(ChatColor.GREEN + "You successfully left " + ChatColor.BLUE + guild.getGuildName() + ChatColor.GREEN + " guild!");
        Player guildLeader = javaPlugin.getServer().getPlayer(guild.getOwnerUUID());
        guildLeader.sendMessage(ChatColor.BLUE + "Player " + ChatColor.GREEN + player.getDisplayName() + ChatColor.BLUE + " left your guild!");

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
