package org.bitbucket.arvade.mcguilds.command;

import com.google.inject.Inject;
import lombok.Setter;
import org.bitbucket.arvade.mcguilds.factory.MemberFactory;
import org.bitbucket.arvade.mcguilds.model.Guild;
import org.bitbucket.arvade.mcguilds.model.GuildMember;
import org.bitbucket.arvade.mcguilds.model.Invite;
import org.bitbucket.arvade.mcguilds.service.GuildMemberService;
import org.bitbucket.arvade.mcguilds.service.GuildService;
import org.bitbucket.arvade.mcguilds.service.InviteService;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.Optional;

public class AcceptInvite implements NamedCommandExecutor {

    private final String DEFAULT_NAME = "acceptinvite";

    @Setter
    private String commandName = DEFAULT_NAME;

    private InviteService inviteService;
    private GuildService guildService;
    private MemberFactory memberFactory;
    private GuildMemberService guildMemberService;

    @Inject
    public AcceptInvite(InviteService inviteService, GuildService guildService, MemberFactory memberFactory, GuildMemberService guildMemberService) {
        this.inviteService = inviteService;
        this.guildService = guildService;
        this.memberFactory = memberFactory;
        this.guildMemberService = guildMemberService;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command!");
            return true;
        }
        if (args.length != 1) {
            commandSender.sendMessage(ChatColor.RED + "Wrong amount of arguments!");
            return false;
        }
        Player target = (Player) commandSender;
        String guildName = args[0];
        Optional<Guild> guildOptional = guildService.findByName(guildName);

        if (!guildOptional.isPresent()) {
            target.sendMessage(ChatColor.RED + "Guild " + ChatColor.BLUE + guildName + ChatColor.RED + " doesn't exists!");
            return true;
        }
        Guild guild = guildOptional.get();

        Optional<Guild> targetHasGuild = guildService.findByUUID(target.getUniqueId());
        if (targetHasGuild.isPresent()) {
            Guild targetsGuild = targetHasGuild.get();
            target.sendMessage(ChatColor.RED + "You're already owner of " + ChatColor.BLUE + targetsGuild.getGuildName() + ChatColor.RED + " guild!");
            return true;
        }

        Optional<GuildMember> targetIsMember = guildMemberService.findByUUID(target.getUniqueId());
        if (targetIsMember.isPresent()) {
            GuildMember targetGuildMember = targetIsMember.get();
            if (targetGuildMember.getGuild() != null) {
                target.sendMessage(ChatColor.RED + "You're already member of " + ChatColor.BLUE + targetGuildMember.getGuild().getGuildName() + ChatColor.RED + " guild!");
                return true;
            }
        }

        Optional<Invite> inviteOptional = inviteService.findLatestInvite(target.getUniqueId(), guild);
        if (inviteOptional.isPresent()) {
            Invite invite = inviteOptional.get();
            Date currentDate = new Date();
            long diff = currentDate.getTime() - invite.getRequestDate().getTime();
            long diffInMinutes = (diff / (60000));

            if (diffInMinutes < 5) {
                memberFactory.createGuildMember(target, guild);
                invite.setAccepted(true);
                invite.setResponseDate(currentDate);
                inviteService.persist(invite);
            } else {
                target.sendMessage(ChatColor.RED + "Invitation from " + ChatColor.BLUE + guild.getGuildName() + ChatColor.RED + " is no longer up to date!");
            }
        } else {
            target.sendMessage(ChatColor.RED + "You're not invited by " + ChatColor.BLUE + guild.getGuildName() + " guild!");
        }

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
