package org.bitbucket.arvade.mcguilds.command;

import lombok.Setter;
import org.bitbucket.arvade.mcguilds.factory.GuildFactory;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.inject.Inject;

public class CreateGuildCommand implements NamedCommandExecutor {

    private final String COMMAND_NAME = "createguild";

    @Setter
    private String commandName = COMMAND_NAME;

    private GuildFactory guildFactory;

    @Inject
    public CreateGuildCommand(GuildFactory guildFactory) {
        this.guildFactory = guildFactory;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must be player to use this command");
            return true;
        }
        if (args.length != 1) {
            return false;
        }

        String outputMessage = ChatColor.GREEN + "You created" + ChatColor.BLUE + " %s" + ChatColor.GREEN + ", good luck!";
        Player owner = (Player) commandSender;
        String guildName = args[0];

        if (guildName.length() > 10) {
            owner.sendMessage(ChatColor.RED + "Guild's name should contain maximum 10 characters!");
            return true;
        }

        if (guildFactory.createGuild(owner, guildName)) {
            owner.sendMessage(String.format(outputMessage, guildName));
        }

        return true;
    }

    @Override
    public String getName() {
        return commandName;
    }
}
