package org.bitbucket.arvade.mcguilds.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bitbucket.arvade.mcguilds.converter.UUIDConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Entity
@Getter
@Setter
public class Invite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    @Convert(converter = UUIDConverter.class)
    private UUID invitator;

    @Column(nullable = false)
    @Convert(converter = UUIDConverter.class)
    private UUID target;

    @Column
    private boolean isAccepted = false;

    @Column
    private Date requestDate = new Date();

    @Column
    private Date responseDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Guild_ID")
    private Guild guild;

    public Invite(UUID invitator, UUID target) {
        this.invitator = invitator;
        this.target = target;
    }
}
