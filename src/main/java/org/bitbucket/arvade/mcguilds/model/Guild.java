package org.bitbucket.arvade.mcguilds.model;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.arvade.mcguilds.converter.UUIDConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity(name = "Guild")
@Getter
@Setter
public class Guild {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String guildName;

    @Column
    @Convert(converter = UUIDConverter.class)
    private UUID ownerUUID;

    @Column
    private String ownerNickname;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @Column
    private List<GuildMember> members;

    @Column
    private Date creationDate = new Date();

    @Column
    private boolean isClosed = false;
}
