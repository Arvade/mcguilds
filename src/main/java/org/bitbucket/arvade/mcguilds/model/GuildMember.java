package org.bitbucket.arvade.mcguilds.model;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.arvade.mcguilds.converter.UUIDConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Setter
@Getter
public class GuildMember {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Convert(converter = UUIDConverter.class)
    private UUID playerUUID;

    @Column
    private String nickname;

    @Column
    private int meritPoints = 0;

    @Column
    private Date joinDate = new Date();

    @ManyToOne
    @JoinColumn(name = "Guild_ID")
    private Guild guild;
}
